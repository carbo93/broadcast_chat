<?php

use App\Events\MessagePosted;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'cors'], function () {
    
    Route::get('/messages/{roomid}', function ($roomid) {
        return App\Message::where('roomid',$roomid)->with('user')->get();
    })->middleware('auth');

    Route::post('/messages', function () {
        // Store the new message
        $user = Auth::user();

        $message = $user->messages()->create([
            'message' => request()->get('message'),
            'roomid' => request()->get('roomid')
        ]);

        // Announce that a new message has been posted
        broadcast(new MessagePosted($message, $user))->toOthers();

        return ['status' => 'OK'];
        
    })->middleware('auth');
    Route::get('/home', 'HomeController@index');
    
    Route::get('/', function () {
        return view('welcome');
    });


    Auth::routes();
});







