# BroadCast Chat Demo with Laravel 5.6, VueJS, and Socket.io

[參考處](https://laravel-china.org/articles/5121/a-small-demo-based-on-laravelvueredis-real-time-chat).

## Development

1. Clone or fork this repository
1. Run `composer install`
1. Run `php artisan key:generate`
1. Fill out relevant items in your `.env` file, including:

    ```
    DB_CONNECTION
    REDIS
    ```

1. Run `npm i`
1. Build assets with `npm run dev`
1. Use `laravel-echo-server start` to start the server
1. Use `php artisan queue:listen` to start the queue server
## Important Notes


- if you want to check redis run 
`redis-cli
127.0.0.1:6379>monitor`
- You will need to set your environment variables in the `.env` file! Don't forget to change the broadcasting settings to Socket.io, for example.