@extends('layouts.app')

@section('content')

<div id="app" class='app'>
    <div class="container">
         <main-app></main-app>
    </div>
</div>
@endsection
