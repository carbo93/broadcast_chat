import Userlist from './components/UserList.vue'
import Broadcast from './components/Broadcast.vue'

const routes = [
    { path: '/', component: Userlist, name: 'Index' },
    { path: '/Userlist', component: Userlist, name: 'Userlist', props: true},
    { path: '/Broadcast/:id', component: Broadcast, name: 'Broadcast', props: true },
    { path: '*', redirect: '/', name: 'None'},

];

export default routes;